from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .forms import QuestionForm, AnswerForm
from .models import Question
from django.core.paginator import Paginator


def index(request):
    """
    eboard 목록 출력
    """
    # 입력 파라미터로 page 를 가져올때 사용 page 값이 없으면 1 설정
    page = request.GET.get('page', '1')

    question_list = Question.objects.order_by('-create_date')

    # 페이징처리, Paginator 는 리스트를 페이징 객체로 변환
    paginator = Paginator(question_list, 5)
    # 페이지당 5개씩 보여주기
    page_obj = paginator.get_page(page)

    context = {'question_list': page_obj}

    return render(request, 'eboard/question_list.html', context)


def detail(request, question_id):
    """
    eboard 내용 출력
    """
    # question = Question.objects.get(id=question_id)
    # 질문조회시 해당 질문이 없을때 404 error 를 화면에 출력하게 바꾼다.
    question = get_object_or_404(Question, pk=question_id)
    context = {'question': question}
    return render(request, 'eboard/question_detail.html', context)


def answer_create(request, question_id):
    """
    eboard 답변 등록
    """
    question = get_object_or_404(Question, pk=question_id)
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.create_date = timezone.now()
            answer.question = question
            answer.save()
            return redirect('eboard:detail', question_id=question.id)
    else:
        form = AnswerForm()
    context = {'question': question, 'form': form}
    return render(request, 'eboard/question_detail.html', context)


def question_create(request):
    """
    eboard 질문 등록
    """
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.create_date = timezone.now()
            question.save()
            return redirect('eboard:index')
    else:
        form = QuestionForm()
    context = {'form': form}
    return render(request, 'eboard/question_form.html', context)
